#!/usr/bin/python

import sys
import urllib2
import re
import cgitb
import cgi
import codecs
import json
cgitb.enable()

sys.stdout = codecs.getwriter('utf-8')(sys.stdout);

from lxml.html import document_fromstring
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options

cache_opts = {
	'cache.type': 'file',
	'cache.data_dir': '/tmp/kdcache/data',
	'cache.lock_dir': '/tmp/kdcache/lock'
}

cache = CacheManager(**parse_cache_config_options(cache_opts))

class FetchPage:
	@cache.cache('cached_get_page', expire=360000)
	def get_page(self, url):
		return urllib2.urlopen(url).read()

f = FetchPage();

form = cgi.FieldStorage()
rid = form['Id'].value
page = f.get_page("http://www.parallelkingdom.com/Kingdom.aspx?kingdomid=Kng" + rid)
root = document_fromstring(page)

name = root.cssselect('#MainContent_KingdomBody_kingdomNameHeader')[0].text_content()

kid = re.compile('.*Kng')


out = '{"src": {"Id":"' + rid + '", "Name": ' + json.dumps(name, ensure_ascii=False) + '}, "links": ['
for a in root.cssselect('#MainContent_KingdomBody_allyListTable a'):
	out += '{"Id": "' + kid.sub('', a.get('href')) + '", "Name": ' + json.dumps(a.text_content().strip(), ensure_ascii=False) + '},'
out = out.rstrip(',')
out += ']}'
print "Content-Type: application/json"
print 

sys.stdout.write(out)
