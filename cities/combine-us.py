#!/usr/bin/python

import csv
import sys

state = sys.argv[1]

f = open('pkcities_2011-11-11_21-55.csv', 'r')
icsv = csv.reader(f, delimiter=';')

of = open(state + '.csv', 'w')
ocsv = csv.writer(of)

for r in icsv:
	if r[1].strip() == state:
		r.append(r[2].strip() + ', ' + r[1].strip())
		ocsv.writerow(r)
