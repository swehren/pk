#!/usr/bin/python

import re
import csv

f = open('cities3', 'r')

of = open('built.csv', 'w')
ocsv = csv.writer(of)

ids = [];
names = [];
dates = [];

state = 'dates'
dateMatch = re.compile('^[0-9]{1,2}/[0-9]{1,2}/20[0-9]{2}$')

for idx, r in enumerate(f):
	if r.strip() == 'GetListPage1':
		if len(ids) != len(names) != len(dates):
			print "Count Mismatch: " + str(len(ids)) + ' ' + str(len(names)) + ' ' + str(len(dates)) + ' ' + str(idx)
		else:
			for idx2, c in enumerate(ids):
				row = [ ids[idx2], names[idx2], dates[idx2]]
				ocsv.writerow(row)

		if state != 'dates':
			print 'Unexpected GetListPage1:' +state + ' '  + str(idx)

		ids = []
		names = []
		dates = []
		state = 'PlayerResults'
		continue

	if r.strip() == 'PlayerResults':
		if state != 'PlayerResults':
			print 'Unexpected PlayerResults: ' + str(idx)

		state = 'ids'
		continue

	if state == 'ids':
		if r.find('Cty') == 0:
			ids.append(r.strip())
			continue
		else:
			state = 'names'
	
	if state == 'names':
		if dateMatch.match(r.strip()) == None:
			names.append(r.strip())
			continue
		else:
			state = 'dates'
	
	if state == 'dates':
		if dateMatch.match(r.strip()) != None:
			dates.append(r.strip())
			continue
		else:
			print "Bad Date: " + str(idx)
