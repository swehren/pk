#!/usr/bin/python

import csv

f = open('gout2.csv', 'r')
icsv = csv.reader(f, delimiter=',')

of = open('markers.js', 'w')

of.write('markers = [];\n')

for idx, r in enumerate(icsv):
	if len(r) > 9:
		of.write('var latLng' + str(idx) + ' = new google.maps.LatLng(' + r[8] + ',' + r[9] + ');\n')
		of.write('var marker = new google.maps.Marker({ position: latLng' + str(idx) + ', title: "' + r[2].strip() + '", flat: true });\n')
		of.write('markers.push(marker);\n')
		of.write('attachWindow(marker, \'<h2>' + r[7] + '</h2><a href="http://parallelkingdom.com/City.aspx?cityid=' + r[3].strip() + '">View Profile</a>\');\n')
of.write('var cluster = new MarkerClusterer(map, markers, {maxZoom: 16});')

