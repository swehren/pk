#!/usr/bin/python

import csv, os, sys
from pygeocoder import Geocoder

source = sys.argv[1]

f = open(source, 'r')
icsv = csv.reader(f, delimiter=',')

of = open(os.path.splitext(source)[0] + '-coded.csv', 'w')
ocsv = csv.writer(of)

for r in icsv:
	if len(r) < 9:
		try:
			res = Geocoder.geocode(r[7])
			r.append(res[0].coordinates[0])
			r.append(res[0].coordinates[1])
		except:
			print "Error:" + r[2]
	ocsv.writerow(r)
