#!/usr/bin/python

import csv
import sys

country = sys.argv[1]
f = open('pkcities_2011-11-11_21-55.csv', 'r')
icsv = csv.reader(f, delimiter=';')

of = open(country + '.csv', 'w')
ocsv = csv.writer(of)

for r in icsv:
	if r[0].strip() == country:
		r.append(r[2].strip() + ', ' + r[0].strip())
		ocsv.writerow(r)
