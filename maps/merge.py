#!/usr/bin/python

import sys
from xml.etree.ElementTree import ElementTree

f1 = sys.argv[1]
f2 = sys.argv[2]
out = sys.argv[3]

tree1 = ElementTree()
tree2 = ElementTree()

tree1.parse(f1);
tree2.parse(f2);

root = tree1.getroot().find('Document');

for i in tree2.find('Document').getchildren():
	if i.tag == 'name':
		continue

	if i.tag == 'Style':
		if root.find("Style[@id='" + i.attrib['id'] + "']") == None:
			root.insert(1,i)
		continue
	root.append(i)

tree1.write(out)
