#!/usr/bin/python

import sys, argparse, re
from xml.etree.ElementTree import ElementTree

removes = re.compile('(.*(Hardened Flag|Flag - |Iron Cave|Stone Cave|Crystal Hut|Garrison Post - |Oil Well|Research Lab|Armory - |Siege Factory - ))|(^(Crystal Patch|Troll Hut)$)')

parser = argparse.ArgumentParser(description='Merge and clean multiple PK kml files')
parser.add_argument('files', nargs='+')
parser.add_argument('--out', '-o', dest='out', required=True)

args = parser.parse_args()

f1 = 'template.kml'
tree1 = ElementTree()
tree1.parse(f1)
root = tree1.getroot().find('Document')

for file in args.files:
	f2 = file
	tree2 = ElementTree()
	tree2.parse(f2);

	for i in tree2.find('Document').getchildren():
		if i.tag == 'name':
			continue

		if i.tag == 'Style':
			if root.find("Style[@id='" + i.attrib['id'] + "']") == None:
				root.insert(1,i)
			continue

		if removes.match(i.find('name').text) != None:
			continue

		placemarks = i.findall('Placemark')
		for p in placemarks:
			g = p.find('MultiGeometry')
			poly = p.find('Polygon')
			if g != None or poly != None:
				i.remove(p);

		root.append(i)

tree1.write(args.out, xml_declaration=True)
