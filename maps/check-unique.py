#!/usr/bin/python

import sys
from xml.etree.ElementTree import ElementTree

f1 = sys.argv[1]
f2 = 'template.kml'
out = sys.argv[2]

tree1 = ElementTree()
tree2 = ElementTree()

tree1.parse(f1);
tree2.parse('template.kml');

root = tree1.getroot().find('Document');
oroot = tree2.getroot().find('Document');

for i in root.getchildren():

	if i.tag == 'Style':
		if oroot.find("Style[@id='" + i.attrib['id'] + "']") == None:
			oroot.insert(1,i)
		continue
	oroot.append(i)


tree2.write(out)
