<!-- The Identity Transformation -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- Whenever you match any node or any attribute -->
  <xsl:template match="node()|@*">
	  <!-- Copy the current node -->
	  <xsl:copy>
		  <!-- Including any attributes it has and any child nodes -->
		  <xsl:apply-templates select="@*|node()"/>
	  </xsl:copy>
  </xsl:template>

  <xsl:template match="Placemark[Polygon|MultiGeometry]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Kingdom Tower')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Hardened Flag')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Iron Cave')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Stone Cave')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Flag - ')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Crystal Hut - ')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Garrison Post - ')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Oil Well')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Research Lab')]]">
  </xsl:template>
  <xsl:template match="Folder[name[contains(.,'Vacation House - ')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,' - Roc Shrine - ')]]">
  </xsl:template>
  <xsl:template match="Folder[name[contains(.,'Armory - ')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'Siege Factory - ')]]">
  </xsl:template>

  <xsl:template match="Folder[name[contains(.,'City House - ')]]">
  </xsl:template>
  <xsl:template match="Folder[name = 'Crystal Patch']">
  </xsl:template>
  <xsl:template match="Folder[name = 'Cavern']">
  </xsl:template>
  <xsl:template match="Folder[name = 'Troll Hut']">
  </xsl:template>


  <xsl:template match="Folder[name = 'Roc Nest']">
  </xsl:template>

</xsl:stylesheet>
