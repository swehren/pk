<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title></title>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
		var wrapper;
		google.load('visualization', '1.0', {'packages':['corechart', 'charteditor']});
		google.setOnLoadCallback(drawChart);
		function drawChart() {
			var data = new google.visualization.DataTable();
			data.addColumn('datetime','Time');

			scores = [<?php echo file_get_contents('scores.js'); ?> ];
			for(var key in scores[0].scores) {
				data.addColumn('number', key);
			}

			for(var i = 0; i < scores.length; i++) {
				var s = scores[i];
				date = new Date();
				date.setTime( s.time * 1000 )
				var row = [ date ];
				for(var key in scores[0].scores) {
					row.push(s.scores[key]);
				}
				data.addRow(row);
			}
			

			data.sort(0);
			wrapper = new google.visualization.ChartWrapper({
				chartType: 'ScatterChart',
				dataTable: data,
				options: {'title': 'Brick Race', width: '100%', height: '100%', lineWidth: 1, pointSize: 3, vAxis: {viewWindowMode: 'explicit', viewWindow: { min: 20000 }} },
				containerId: 'chart'
			});
			wrapper.draw();
		}

		function openEditor() {
			// Handler for the "Open Editor" button.
			var editor = new google.visualization.ChartEditor();
			google.visualization.events.addListener(editor, 'ok',
				function() { 
					wrapper = editor.getChartWrapper();  
					wrapper.draw(document.getElementById('visualization')); 
				}); 
			editor.openDialog(wrapper);
		}

	</script>
	<style>
		body, html, #chart { margin: 0; padding: 0; height: 100%; position: relative; }
	</style>

</head>
<body>
	<div id="chart"></div>
	<div id="edit" onclick="openEditor()">Edit</div>
</body>
</html>

