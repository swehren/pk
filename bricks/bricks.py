#!/usr/bin/python

import sys, codecs, argparse, os, stat
from lxml.html import document_fromstring

sys.stdout = codecs.getwriter('utf-8')(sys.stdout);

parser = argparse.ArgumentParser(description='Merge and clean multiple PK kml files')
parser.add_argument('--time', '-t', dest='time', type=int, required=False)
parser.add_argument('file', type=str)
args = parser.parse_args()

if(args.time == None):
	args.time = int(round(os.stat(args.file).st_ctime))

f = open(args.file)
body = f.read()
root = document_fromstring(body)

players = root.cssselect('#MainContent_KingdomBody_membersList tr')

scores = {}

for p in players:
	try:
		scores[p[0].text_content().strip()] = int(p[4].text_content().strip())
	except ValueError:
		pass

print '{ "time": ' + str(args.time) + ', scores: {'
for key, value in sorted(scores.iteritems(),reverse=True, key=lambda (k,v): (v,k)):
	format = "'%s': %s, "
	print format % (key, value)
print '}},'
